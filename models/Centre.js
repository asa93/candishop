const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
   centre : String ,
   departement:String,
   geoloc: Object
});
const Centre = mongoose.model('centre', schema, 'centre');

module.exports = Centre;
