const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
   infos : Object ,
   email:String,
   retrievedDate : { type: Date, default : Date.now}
});
const Infos = mongoose.model('infos', schema, 'infos');

module.exports = Infos;
