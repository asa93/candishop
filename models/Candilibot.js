const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
  tenant: String,
  email: String,
  userId: String,
  centre: String,
  id:String,
  subscriptionDate : { type: Date, default : Date.now},
  tenant : {type : String, default : null},
  link : {type : String, default : null},
  token : String,
  isValid : {type : Boolean, default : false},
  booked : {type : Boolean, default : null},
  lastCheck:{ type: Date, default : null},
  bookedDate:{ type: Date, default : null}
});




const Candilibot = mongoose.model('Crawl', usersSchema, 'Crawl');

module.exports = Candilibot;
