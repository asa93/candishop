 
const email_html = require('./email.js');
var schedule = require('node-schedule')
var request = require('request-promise')
const fs = require('fs');
const Candilibot = require('./models/Candilibot');
const Centre = require('./models/Centre');
const Infos = require('./models/Infos');
const mongoUtil = require('./config/mongo');
const proxy = require('./proxy.js')
const NodeCache = require( "node-cache" );
const myCache = new NodeCache({checkperiod: 600 });
var HttpsProxyAgent = require('https-proxy-agent');
async function sleep(milliseconds)  {
  milliseconds =  (1-Math.random()/10) * milliseconds

  let date = new Date()
  console.log("[", date.getHours(), "h", date.getMinutes(),"min] - waiting ", milliseconds/1000, " seconds.")
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// DB setup
// TODO: Use winstonjs logger to log messages like this not console.log.
mongoUtil.connectToServer();

var GMAIL_AUTH = null;
//https://beta.interieur.gouv.fr/candilib/candidat?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZWY5MzM2Y2RiOTgxMDAxMTAwYmRmMiIsImxldmVsIjowLCJpYXQiOjE2MDcyNzA3NDIsImV4cCI6MTYwNzUyOTk0Mn0.JISTFZYkpLbHekHk7t8enFMsynBOmmcAE2dIcrRMNLs
// Load client secrets from a local file.
fs.readFile('credentials.json', (err, content) => {
  if (err) return console.log('Error loading client secret file:', err);
  // Authorize a client with credentials, then call the Gmail API.
  email_html.authorize(JSON.parse(content), async (auth)=>{
      GMAIL_AUTH = auth;
/*
	 let res = await email_html.sendMail(GMAIL_AUTH, 'immoji.contact@gmail.com', 'Immoji'
	   "<strong>Hello World ! I'm now ready to send notifications </strong>")
	console.log("///////RES", res)
	*/
	});
});

async function bookPlace(departement, centre, creneau,userId,token,email){
  // let res = await proxy.rotateProxy();
  //let myProxy = await new HttpsProxyAgent(res.proxy);
  //let userAgent = res.userAgent;
console.log("toto",departement, centre, creneau,userId,token)
    //creneau "2021-01-14T08:00:00.000+01:00"
   try{
     let book= await   
   request(
     {
     url : "https://beta.interieur.gouv.fr/candilib/api/v2/candidat/places",
     method :"PATCH",
    // agent:myProxy,
     headers : {
       "content-type": "application/json",
       "X-USER-ID" : userId,
       "Authorization" : "Bearer "+token
     },
     body: {"nomCentre":centre
     ,"geoDepartement": departement
     ,"date": creneau
     ,"isAccompanied":true
     ,"isModification": (email=="habyba-9@outlook.fr" || email=="c.attoungbre@yahoo.fr" || email=="burtonterry@outlook.fr")?true:false
     ,"hasDualControlCar":true},
     json: true
   }
 )     
//set booked


 deleteUserFromCache(userId)
 await Candilibot.updateMany({userId:userId}, {$set: {booked:true,bookedDate:Date.now()}})

 console.log("SUCCCESS", book)
 setBooked(email,centre)

  
  return book
}catch(e){

console.log(e.toString())
/*await email_html.sendMail(GMAIL_AUTH, 'immoji.contact@gmail.com', 'Candishop'
, 'ERROR BOOKING',  
e.toString())*/
return
}

}
async function getPlace(departement,centre,index){
  /*let res = await proxy.rotateProxy();
  let myProxy = await new HttpsProxyAgent(res.proxy);
  let userAgent = res.userAgent;*/
   let Users=myCache.get(centre)
   let users= await myCache.get("DistinctUsers")
   let UsersID=Users.map(e=>e.userId)
  
  // if(users.length>Users.length)
  // users=await users.filter(e=>UsersID.indexOf(e.userId)<0)
  //  console.log(users.length)
   let id = index%users.length//await Math.round((users.length-1) * Math.random())

   let userId=users[id].userId
   let token=users[id].token
   let places= await   
   request(
     {
     url : "https://beta.interieur.gouv.fr/candilib/api/v2/candidat/places?"
         +"begin= 2020-12-07T12:01:59.699+01:00"
         +"&end= 2021-03-31T23:59:59.999+02:00"
         +"&geoDepartement="+departement
         +"&nomCentre="+centre,
     method :"GET",
     //agent=myProxy,
     headers : {
       "content-type": "application/json",
       "X-USER-ID" : userId,
       "Authorization" : "Bearer "+token
     },
     body: {},
     json: true
   }
 )
 console.log(departement,centre,places)
 var places1=[].concat(places)
 for(i=0;i<places.length && i<Users.length;i++){
  let id = await Math.round((places1.length-1) * Math.random())
  //let idUser = i%Users.length
 
  bookPlace(departement, centre, places1[id],Users[i].userId,Users[i].token, Users[i].email)
  places1.splice(id,1)
 }


}

async function getDepartements(){
  let users= await myCache.get("DistinctUsers")
  let id = await Math.round((users.length-1) * Math.random())
  let userId=users[id].userId
  let token=users[id].token
  /*let res = await proxy.rotateProxy();
  let myProxy = await new HttpsProxyAgent(res.proxy);
  let userAgent = res.userAgent;*/
  
 
   let departements= await   
   request(
     {
     url : "https://beta.interieur.gouv.fr/candilib/api/v2/candidat/departements",
     method :"GET",
     //agent:myProxy,
     headers : {
       "content-type": "application/json",
       "X-USER-ID" : userId,
       "Authorization" : "Bearer "+token
     },
     body: {},
     json: true
   }
 )

return departements

}
async function getCentres(departement){
  let users= await myCache.get("DistinctUsers")
  let id = await Math.round((users.length-1) * Math.random())
  let userId=users[id].userId
  let token=users[id].token
  /*let res = await proxy.rotateProxy();
  let myProxy = await new HttpsProxyAgent(res.proxy);
  let userAgent = res.userAgent;*/
  
 
   let centres= await   
   request(
     {
     url : "https://beta.interieur.gouv.fr/candilib/api/v2/candidat/centres?end=2021-03-31T23%3A59%3A59.999%2B02%3A00&departement="+departement,
     method :"GET",
     //agent:myProxy,
     headers : {
       "content-type": "application/json",
       "X-USER-ID" : userId,
       "Authorization" : "Bearer "+token
     },
     body: {},
     json: true
   }
 )
 console.log(departement)
 for(let i=0; i<centres.length; i++){
 
  let c = centres[i]

  let centre = c.centre.nom
  if(c.count <= 0) continue  
  console.log("centre : ", centre, " - count : ", c.count)
  let botCentre=myCache.get(centre)
  if(botCentre==undefined)
  continue;
  await sleep(100)
  let places = await getPlace(departement, centre)

  console.log(places)

}
return centres

}

async function getUserInformations(userId,token){
  return await   
  request(
    {
    url : "https://beta.interieur.gouv.fr/candilib/api/v2/candidat/me",
    method :"GET",
    headers : {
      "content-type": "application/json",
      "X-USER-ID" : userId,
      "Authorization" : "Bearer "+token
    },
    body: {},
    json: true
  }
)

}
async function getUserId(token){
  //let res = await proxy.rotateProxy();
  //let myProxy = await new HttpsProxyAgent(res.proxy);
  //let userAgent = res.userAgent;
  
  return await   
  request(
    {
    url : "https://beta.interieur.gouv.fr/candilib/api/v2/auth/candidat/verify-token?token="+token,
    method :"GET",
    //agent:myProxy,
    headers : {
      "content-type": "application/json"
    },
    body: {},
    json: true,
    resolveWithFullResponse: true
  }
).then(function(response) {
 return response.headers['x-user-id']
});

}



var validateUsers=async function(){
  var users=await Candilibot.find({"booked":false}).sort({ subscriptionDate : 1})

  for(let i=0;i<users.length;i++){
    let user=users[i]
    try{
      
      let token=user.link.split("token=")[1]
      let userId=await getUserId(token)
      let infos=await getUserInformations(userId,token)
      
      await insertInfos(infos,user.email)
      isValid(user.email,true)
    console.log(await Candilibot.updateOne({_id:user._id}, {$set: {userId:userId,token:token,isValid:true,lastCheck:Date.now()}}))
    
    }catch(e){
      console.log(e)
      await Candilibot.updateOne({_id:user._id}, {$set: {isValid:false,lastCheck:Date.now()}})
      isValid(user.email,false)
     /* await email_html.sendMail(GMAIL_AUTH, user.email, 'CandiliBot'
      , "Ton lien de connexion n'est plus valide :( ",  
       "Viens sur https://candilibot.fr ")*/
       
    }
    await sleep(1000)
  };
  return
}


var populateCache=async function(){
    //set all the valid users in the cache !!! 
    let res=await Candilibot.find({"isValid":true,"booked":false}).sort({ subscriptionDate : 1})
    let res1=await Candilibot.find({"isValid":true}).sort({ subscriptionDate : 1})
    result =await  res1.filter(function (a) {
      var key = a.userId + '|' + a.token;
      if (!this[key]) {
          this[key] = true;
          return true;
      }
       }, Object.create(null));
    await myCache.set("DistinctUsers",result,  10000)
    let groupsCentre=await groupBy(res,"centre")
    
    let arrayCache=[]
    for(let i=0;i<Object.keys(groupsCentre).length;i++){
    let key=Object.keys(groupsCentre)[i]
    await arrayCache.push({ key:key,val:groupsCentre[key],ttl: 10000})
    
    }
    //set all the valid users in the cache !!! 
    await myCache.mset(arrayCache)

    //get only centres used
    let centres=await Centre.find({centre:{$in:Object.keys(groupsCentre)}})
    
    //set all the different centers in the cache !!! 
    await myCache.set("centres",centres,  10000)
    let departements=await Centre.distinct("departement",{centre:{$in:Object.keys(groupsCentre)}})
    console.log(departements)
    await myCache.set("departement",departements,  10000)
    return
}
var deleteUserFromCache=async function(userId){

  let centres=await myCache.get("centres")
  for(let i=0;i<centres.length;i++){

  let centre=await centres[i].centre
  let usersCentre=await myCache.get(centre)
  if(usersCentre!=undefined){
  let filteredUsers=await usersCentre.filter(e=>e.userId!=userId)
  myCache.set(centre,filteredUsers,  10000) 
  }
  }

}


var groupBy = function(xs, key) {

  return xs.reduce(function(rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  
  }, {});

};

var insertInfos=async function(infos,email){
  if(!(await Infos.count({infos:infos,email:email})))
  await Infos.create({infos:infos,email:email})
}

var init=async function(){
  let token="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVkZWY5MzM2Y2RiOTgxMDAxMTAwYmRmMiIsImxldmVsIjowLCJpYXQiOjE2MDcyNzA3NDIsImV4cCI6MTYwNzUyOTk0Mn0.JISTFZYkpLbHekHk7t8enFMsynBOmmcAE2dIcrRMNLs"
 var userId=await getUserId(token)
console.log(userId)//await getUserInformations(userId,token))
}








queryByCentre=async function(){
  let centres=myCache.get("centres")
 // var a=0
  for(let i=0;i<centres.length ;i++){
   // console.log(i)
    let centre=centres[i].centre
    let departement=centres[i].departement
    let botCentre=myCache.get(centre)
    //await sleep(100)
    if(botCentre!=undefined){
    
    getPlace(departement, centre,i)
    
    }

  }
}


queryByDepartment=async function(){
 
var departement =await myCache.get("departement")

 var centres = []
 for(let i=0; i<departement.length; i++){
   
  centres = centres.concat(  getCentres(departement[i]))
  //console.log(centres)
  console.log(i)
 }

}
populateCentres=async function(){
  let departements = await getDepartements()
  departements=departements.geoDepartementsInfos
  console.log(departements)
  for(let i=0; i<departements.length; i++){
    let centres=await getCentres(departements[i].geoDepartement)
    for(let a=0; a<centres.length; a++){
      let centre=centres[a].centre 
      let count= await Centre.count({centre:centre.nom,departement:parseInt(centre.geoDepartement)})
      if(count<1){
        console.log(centre)
        Centre.create({centre:centre.nom,departement:centre.geoDepartement,geoloc:{coordinates :centre.geoloc.coordinates,type:"Point"}})
      }

    }
  }
  return true
}
GetBubbleBot=async function(){
  var cursor=0
  var remaining=100
  var bots=[]
  console.log(cursor)
  while(remaining>0){
    console.log(cursor)
   let res =await   
   request(
     {
     url : "https://candilibot.fr/api/1.1/obj/bot?cursor="+cursor,
        
     method :"GET",
     //agent=myProxy,
     headers : {
       "content-type": "application/json"
     },
     body: {},
     json: true
   }
 )
 bots=bots.concat(res.response.results)
 cursor= cursor+res.response.count
 remaining=res.response.remaining
  }


 return bots
}





async function isValid(email,bool){
 
  let places= await   
  request(
    {
    url : "https://candilibot.fr/api/1.1/wf/isValid?email="+email+"&bool="+bool,
    method :"POST",
    //agent=myProxy,
    headers : {
      "content-type": "application/json",
      "Authorization" : "Bearer c5d018559fcdb8bbe34e5e284fb56460"
    },
    body: {},
    json: true
  }
)
 }

async function setBooked(email,centre){
 
   let places= await   
   request(
     {
     url : "https://candilibot.fr/api/1.1/wf/setBooked?id1="+email+"&centre="+centre,
     method :"POST",
     //agent=myProxy,
     headers : {
       "content-type": "application/json",
       "Authorization" : "Bearer c5d018559fcdb8bbe34e5e284fb56460"
     },
     body: {},
     json: true
   }
 )
  }


  async function linkNotValid(email){
 
    let places= await   
    request(
      {
      url : "https://candilibot.fr/api/1.1/wf/linkNotValid?email="+email,
      method :"POST",
      headers : {
        "content-type": "application/json",
        "Authorization" : "Bearer c5d018559fcdb8bbe34e5e284fb56460"
      },
      body: {},
      json: true
    }
  )

   }

async function sendMailToUserNonValid(){
console.log("send Mail to non valid")

  let booked=await Candilibot.find({"isValid":false,"booked":false})
   
  result =await  booked.filter(function (a) {
    var key = a.email;
    if (!this[key]) {
        this[key] = true;
        return true;
    }
     }, Object.create(null));
 let email=result.map(e=>e.email)

 for(i=0;i<email.length;i++){
  await linkNotValid(email[i])
  }

} 


async function test(){
 
  
}

//test()

//set old booked to false cause the link gets unvalid the next day
async function oldBooked(){
  var d = new Date();
  d.setHours(0,0,0,0);

  let booked=await Candilibot.find({"booked":true,bookedDate:{"$lt":d}}).sort({ subscriptionDate : 1})
   
  result =await  booked.filter(function (a) {
    var key = a.email;
    if (!this[key]) {
        this[key] = true;
        return true;
    }
     }, Object.create(null));
 let email=result.map(e=>e.email)

 for(i=0;i<email.length;i++){
  await Candilibot.updateMany({"email":email[i]}, {$set: {isValid:false}})
  isValid(email,false)
  }

}

//set old booked to not valid link
var BookedFromYesterdayNotValid = schedule.scheduleJob("0 0 0 * * *", async function(){
oldBooked()
})


var sendMailTo = schedule.scheduleJob("30 16 09 * * *", async function(){
  
  sendMailToUserNonValid()

  })
  

//check newly added user informations
var check = schedule.scheduleJob("*/10 41 10 * * *", async function(){
  //if new centres or departements populates centres table from mongodb
  console.log("win")
 
  //validateUsers informations 
  //TODO : validate from JWT token expiration date and query refresh link and send MAIL if expired
  await validateUsers()
  try{
    await populateCentres()
    }catch(e){
      
    }
  try{
  await populateCache()

  }catch(e){
    console.log(e)
  }
  //console.log(await myCache.get("centres"))
 });
  

  //check new users from bubble and add them to mongodb new users
var firsts = schedule.scheduleJob("30 54 * * * *", async function(){
//query bubble
let bots=await GetBubbleBot()

for(let i=0;i<bots.length;i++){
 
  let count=0
  if(bots[i].booked_boolean==undefined || bots[i].booked_boolean==false){

  count=await Candilibot.count({"id":bots[i].id_text,centre:bots[i].centre_examen_text.split(" - ")[1]})
  console.log(count)  
  
  }
  if(count<1){
    if(bots[i].booked_boolean==undefined || bots[i].booked_boolean==false){
    await Candilibot.create({"id":bots[i].id_text,link:bots[i].lien_text,centre:bots[i].centre_examen_text.split(" - ")[1],email:bots[i].email_text,booked:false})
    await Candilibot.updateMany({"id":bots[i].id_text}, {$set: {link:bots[i].lien_text,booked:false}})
    }
  }else{
    if(bots[i].booked_boolean==undefined || bots[i].booked_boolean==false)
    await Candilibot.updateMany({"id":bots[i].id_text}, {$set: {link:bots[i].lien_text,booked:false}})
  }
}
await validateUsers()
try{
  await populateCentres()
  }catch(e){
    
  }
try{
await populateCache()

}catch(e){
  console.log(e)
}
})


populateCache()


var candishop = schedule.scheduleJob("*/2 * 11-23 * * *", async function(){
  let centres=await myCache.get("centres")
  let departements=await myCache.get("departements")
  let users=await myCache.get("DistinctUsers")
  console.log(centres.length,users.length)
  if(centres.length*2<=users.length){
    
    queryByCentre()
  }else{
    queryByDepartment()
  }

    
});

