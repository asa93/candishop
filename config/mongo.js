const mongoose = require('mongoose');
const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  autoIndex: true,
  reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
  reconnectInterval: 1000, // Reconnect every 500ms
  bufferMaxEntries: 0,
  connectTimeoutMS: 30000, // Give up initial connection after 10 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
}
module.exports = {
  connectToServer: function() {
    var mongoDB =
      'mongodb://immoji2:xyz123@51.178.183.145:27017/immo?authMechanism=DEFAULT&authSource=immo';
    mongoose.Promise = global.Promise;
    mongoose.connect(mongoDB, options);
    mongoose.connection.on('error', () => {
      console.log("MongoDB connection error. Please make sure MongoDB is running.")
    });
  }
};
